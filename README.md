# PHP from Remi Collet's repository

This Ansible role installs the Remi Collet's PHP repository.

When run, it does the following:
1. installs the repository and its signing key
4. optionally installs a list of remirepo packages

## Installation

These PHP packages depend on several EPEL packages, so the EPEL repository must
be installed as well.

Add the following to a `requirements.yml` or `requirements.yaml` file in your
Ansible project and run `ansible-galaxy install -r requirements.yaml`:

```yaml
roles:
  - name: epel
    scm: git
    src: https://gitlab.uvm.edu/saa/ansible-galaxy/epel.git
  - name: php-remi
    scm: git
    src: https://gitlab.uvm.edu/saa/ansible-galaxy/php-remi.git
```


## Usage

A system can be configured for a single version of PHP via DNF modules, or
potentially multiple simultaneous versions via Software Collections.  Allowing
for the possibility of multiple versions might make it easier to test future
upgrades.

To configure a single version of PHP, place a section like this in your playbook:

```yaml
- name: Set up PHP
  hosts: all
  roles:
    - role: epel
    - role: php-remi
      install_version: "8.2"
      install_packages:
        - php
        - php-pecl-zip
```

To support multiple versions of PHP (now or potentially in the future), place a
section like this in your playbook:

```yaml
- name: Set up PHP
  hosts: all
  roles:
    - role: epel
    - role: php-remi
      install_packages:
        - php82
        - php82-php-pecl-zip
```

### Additional options

Full argument documentation is available via `ansible-doc -t role php-remi`.
